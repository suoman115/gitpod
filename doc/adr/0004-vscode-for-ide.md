# 4. VSCode for IDE

Date: 2023-02-13

## Status

Accepted

## Context

IDEs

## Decision

The change that we're proposing or have agreed to implement.

## Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.
