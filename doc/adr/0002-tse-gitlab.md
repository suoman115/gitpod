# 2. Tse Gitlab

Date: 2023-02-13

## Status

Accepted

## Context

Github is not available for on-premise.
Gitlab can be used in on-premise hosting.
Gitlab is open source.

## Decision

The change that we're proposing or have agreed to implement.

## Consequences

We will use git cloud until MVP version.
Intra team will setup on-premise gitlab server.